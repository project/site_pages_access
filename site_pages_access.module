<?php

/**
 * @file
 * Module file.
 */

/**
 * Implements hook_help().
 */
function site_pages_access_help($path, $arg) {

  switch ($path) {
    case 'admin/help#site_pages_access':
      return check_markup(file_get_contents(dirname(__FILE__) . "/README.txt"));
  }
}

/**
 * Implements hook_menu().
 */
function site_pages_access_menu() {

  $items = [];
  $items['admin/config/site_pages_access'] = array(
    'title' => 'Site pages access',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('site_pages_access_menus_admin'),
    'access arguments' => array('administer site_pages_access_menus configuration'),
    'type' => MENU_NORMAL_ITEM,
  );
  $items['admin/config/site_pages_access/rules'] = array(
    'title' => 'Rules to bypass limited access pages',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('site_pages_access_rules'),
    'access arguments' => array('administer site_pages_access_menus configuration'),
    'type' => MENU_LOCAL_TASK,
    'weight' => -1,
  );
  $items['admin/config/site_pages_access/menus'] = array(
    'title' => 'Limited access pages',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('site_pages_access_menus_admin'),
    'access arguments' => array('administer site_pages_access_menus configuration'),
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -2,
  );
  $items['admin/config/site_pages_access/table'] = array(
    'title' => 'Rules / pages table',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('site_pages_access_table_admin'),
    'access arguments' => array('administer site_pages_access_menus configuration'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 0,
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function site_pages_access_permission() {

  $items = [];

  $items['administer site_pages_access_menus configuration'] = array(
    'title' => 'Administer site pages access configuration',
  );

  foreach (menu_get_menus() as $key => $value) {
    $items['administer ' . $key . ' site_pages_access_menus configuration'] = array(
      'title' => 'Block items of ' . $value . ' menu',
    );
  }

  return $items;
}

/**
 * Implements hook_form().
 */
function site_pages_access_rules($form, &$form_state) {

  $form['site_pages_access_rules'] = array(
    '#type' => 'fieldset',
    '#tree' => TRUE,
    '#title' => t('Rules'),
    "#prefix" => '<div id="site-page-access-rules-wrapper">',
    "#suffix" => '</div>',
  );

  if (!isset($form_state['num_rules'])) {

    $rules = variable_get('site_pages_access_rules', '');

    if (($num_rules = count($rules)) > 1) {
      $form_state['num_rules'] = $num_rules;
    }
    elseif (empty($form_state['num_rules'])) {
      $form_state['num_rules'] = 1;
    }

  }

  for ($i = 1; $i <= $form_state['num_rules']; $i++) {
    $form['site_pages_access_rules'][$i] = array(
      '#type' => 'fieldset',
      '#title' => t('Rule') . ' ' . $i,
      '#collapsible' => TRUE,
    );
    $form['site_pages_access_rules'][$i]['availibility'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enabled'),
      '#default_value' => is_array($rules) ? $rules[$i]['availibility'] : TRUE,
    );
    $form['site_pages_access_rules'][$i]['title'] = array(
      '#type' => 'textfield',
      '#title' => t('Title'),
      '#default_value' => is_array($rules) ? $rules[$i]['title'] : 'Rule ' . $i,
      '#states' => array(
        'enabled' => array(
          ':input[name="site_pages_access_rules[' . $i . '][availibility]"]' => array('checked' => TRUE),
        ),
        'disabled' => array(
          ':input[name="site_pages_access_rules[' . $i . '][availibility]"]' => array('checked' => FALSE),
        ),
      ),
    );
    $rules_types_options = array('role' => 'Role');
    drupal_alter('rules_types_options', $rules_types_options);
    $form['site_pages_access_rules'][$i]['rules'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Type'),
      '#options' => $rules_types_options,
      '#default_value' => is_array($rules) ? $rules[$i]['rules'] : array(),
      '#states' => array(
        'enabled' => array(
          ':input[name="site_pages_access_rules[' . $i . '][availibility]"]' => array('checked' => TRUE),
        ),
        'disabled' => array(
          ':input[name="site_pages_access_rules[' . $i . '][availibility]"]' => array('checked' => FALSE),
        ),
      ),
    );
    $form['site_pages_access_rules'][$i]['role'] = array(
      '#type' => 'select',
      '#title' => t('Role'),
      '#options' => array_filter(
        user_roles(),
        function ($val) {

          return ($val != 'administrator');
        }
      ),
      '#default_value' => is_array($rules) ? $rules[$i]['role'] : '',
      '#states' => array(
        'enabled' => array(
          ':input[name="site_pages_access_rules[' . $i . '][availibility]"]' => array('checked' => TRUE),
        ),
        'disabled' => array(
          ':input[name="site_pages_access_rules[' . $i . '][availibility]"]' => array('checked' => FALSE),
        ),
        'visible' => array(
          ':input[name="site_pages_access_rules[' . $i . '][rules][role]"]' => array('checked' => TRUE),
        ),
      ),
    );
  }

  $form['site_pages_access_rules']['add_rule'] = array(
    '#type' => 'submit',
    '#value' => t('Add another rule'),
    '#submit' => array('add_rule'),
    '#ajax' => array(
      'callback' => 'add_remove_rule_callback',
      'wrapper' => 'site-page-access-rules-wrapper',
    ),
  );

  if ($form_state['num_rules'] > 1) {
    $form['site_pages_access_rules']['remove_rule'] = array(
      '#type' => 'submit',
      '#value' => t('Remove latest rule'),
      '#submit' => array('remove_rule'),
      '#ajax' => array(
        'callback' => 'add_remove_rule_callback',
        'wrapper' => 'site-page-access-rules-wrapper',
      ),
    );
  }

  return system_settings_form($form);
}

/**
 * AJAX add.
 */
function add_rule($form, &$form_state) {

  if (!isset($form_state['num_rules'])) {
    $form_state['num_rules'] = 0;
    $form_state['num_rules']++;
  }
  $form_state['num_rules']++;
  $form_state['rebuild'] = TRUE;
}

/**
 * AJAX add/remove callback.
 */
function add_remove_rule_callback($form, $form_state) {

  return $form['site_pages_access_rules'];
}

/**
 * AJAX remove.
 */
function remove_rule($form, &$form_state) {

  if ($form_state['num_rules'] > 1) {
    $form_state['num_rules']--;
  }
  $form_state['rebuild'] = TRUE;
}

/**
 * Implements hook_form().
 */
function site_pages_access_menus_admin($form, &$form_state) {

  $form['site_pages_access_menus'] = array(
    '#tree' => TRUE,
    '#type' => 'fieldset',
    '#title' => t('Menus'),
  );

  $menu_options = menu_get_menus();

  foreach ($menu_options as $key => $value) {
    if (!user_access('administer ' . $key . ' site_pages_access_menus configuration')) {
      unset($menu_options[$key]);
    }
  }

  $form['site_pages_access_menus']['menus'] = array(
    '#title' => t('Choose an availible menus'),
    '#type' => 'checkboxes',
    '#options' => $menu_options,
    '#default_value' => is_array(variable_get('site_pages_access_menus', '')) ? variable_get('site_pages_access_menus', '')['menus'] : array_map(function () { return 0; }, $menu_options),
  );

  foreach ($menu_options as $key => $value) {

    $tree = menu_tree_all_data($key);
    $options = dfs_traverse_menu_tree_options($tree);

    if (count($options) > 0) {

      $form['site_pages_access_menus']['site_pages_access_menu_' . $key . '_items'] = array(
        '#type' => 'fieldset',
        '#title' => $value,
        '#attributes' => array(
          'class' => array('menu-wrapper menu'),
          'id' => $key,
        ),
      );

      $form['site_pages_access_menus']['site_pages_access_menu_' . $key . '_items']['blocked_items'] = array(
        '#type' => 'nested_checkboxes',
        '#options' => $options,
        '#default_value' => is_array(variable_get('site_pages_access_menus', '')) ? variable_get('site_pages_access_menus', '')['site_pages_access_menu_' . $key . '_items']['blocked_items'] : array_map(function () { return 0; }, $options),
      );
    }
  }

  $form['#attached']['js'][] = array(
    'data' => drupal_get_path('module', 'site_pages_access') . '/js/hide_show_menus.js',
    'type' => 'file',
  );

  $form['#attached']['css'][] = array(
    'data' => '.menu-wrapper{ display: none}',
    'type' => 'inline',
  );
  $form['#attached']['css'][] = array(
    'data' => '.menu-wrapper.active{ display: block}',
    'type' => 'inline',
  );

  return system_settings_form($form);
}

/**
 * Implements hook_form().
 */
function site_pages_access_table_admin($form, &$form_state) {

  $form['#tree'] = TRUE;

  $form['site_pages_access_rules_behaviour_politics'] = array(
    '#type' => 'radios',
    '#options' => array(
      'site_wild' => t('Common behaviour for all limited access pages (all the options will be enabled)'),
      'per_page' => t('Different behaviour for each limited access page'),
    ),
    '#default_value' => variable_get('site_pages_access_rules_behaviour_politics', 'site_wild'),
  );

  $rules = variable_get('site_pages_access_rules', '');

  foreach ($rules as $key => $rule) {
    if (!$rule['availibility']) {
      unset($rules[$key]);
    }
  }

  if (!empty($rules)) {

    $rules_options = array_column($rules, 'title');
    $rules_options = array_combine($rules_options, $rules_options);
    $site_pages_access_menus = variable_get('site_pages_access_menus', '');
    $limited_access_pages = array_filter_recursive($site_pages_access_menus, '_not_zero');

    $site_pages_access_tables = variable_get('site_pages_access_table', '');

    foreach ($limited_access_pages['menus'] as $menu_name) {
      $menu = menu_load($menu_name);
      $form['site_pages_access_table'][$menu_name] = array(
        '#type' => 'fieldset',
        '#title' => $menu['title'] . ' items / rules',
      );

      $flatern_limited_access_pages = [];
      $sliced_limited_access_pages = array_slice($limited_access_pages, 1);
      array_walk_recursive(
        $sliced_limited_access_pages,
        function ($v, $k) use (&$flatern_limited_access_pages) {

          $flatern_limited_access_pages[] = $v;
        }
      );

      foreach ($flatern_limited_access_pages as $mlid) {
        $link = menu_link_load($mlid);

        $path = [];
        for ($i = 1; $i <= 9; $i++) {
          if ($link['p' . $i] != 0 && $link['p' . $i] != $link['mlid']) {
            $path[] = menu_link_load($link['p' . $i])['link_title'];
          }
        }
        $path[] = $link['link_title'];

        $default_value = isset($site_pages_access_tables[$menu_name][$mlid]) ? $site_pages_access_tables[$menu_name][$mlid] : array();

        $form['site_pages_access_table'][$menu_name][$mlid] = array(
          '#title' => implode(' > ', $path),
          '#type' => 'checkboxes',
          '#options' => $rules_options,
          '#default_value' => $default_value,
          '#attributes' => array(
            'class' => array(
              'edit-site-pages-access-table',
            ),
          ),
          '#states' => array(
            'checked' => array(
              ':input[name="site_pages_access_rules_behaviour_politics"]' => array('value' => 'site_wild'),
            ),
          ),
        );
      }

    }
  }

  $form['#attached']['js'][] = array(
    'data' => drupal_get_path('module', 'site_pages_access') . '/js/rules_behaviour_politics.js',
    'type' => 'file',
  );

  $form['#submit'][] = '_update_bypass_link_rules_table';
  $form['#submit'][] = '_clear_menu_cache';

  return system_settings_form($form);
}

/**
 * Clear cache.
 */
function _clear_menu_cache(&$form, &$form_state) {

  cache_clear_all(NULL, 'cache_menu');
}

/**
 * Builds a menu tree by depth first search recursive traversal.
 */
function dfs_traverse_menu_tree_options($tree) {

  $options = [];
  foreach ($tree as $item) {
    if (!$item['link']['hidden']) {
      if (count($item['below']) == 0) {
        $options[$item['link']['mlid']] = strip_tags($item['link']['link_title']);
      }
      else {
        $options[$item['link']['mlid']]['#title'] = t(strip_tags($item['link']['link_title']));
        $options[$item['link']['mlid']]['#options'] = dfs_traverse_menu_tree_options($item['below']);
      }
    }
  }

  return $options;
}

/**
 * Implements hook_menu_get_item_alter().
 */
function site_pages_access_menu_get_item_alter(&$router_item, $path, $original_map) {

  global $user;

  $bypass = FALSE;

  if (user_access('administer site configuration', $user)) {
    $bypass = TRUE;
  }
  else {

    $flatern_pages_rules = variable_get('site_pages_access_flatern_pages_rules', '');

    if ($flatern_pages_rules == '') {
      $bypass = TRUE;
    }
    else {

      $mlid = db_select('menu_links', 'ml')
        ->fields('ml', array('mlid'))
        ->condition('link_path', $path)
        ->execute()
        ->fetchField();

      if (isset($flatern_pages_rules[$mlid])) {

        $bypass_aggr = [];
        foreach ($flatern_pages_rules[$mlid] as $rule_title => $rules) {

          foreach ($rules as $rule_type => $value) {
            switch ($rule_type) {
              case 'role':
                $bypass_aggr[$rule_type] = array_key_exists($value, $user->roles);
                break;

              default:
                drupal_alter('site_pages_access_rule_type', $bypass_aggr[$rule_type], $value);
                break;

            }
          }

        }

        if (in_array(FALSE, array_unique($bypass_aggr))) {
          $bypass = FALSE;
        }
        else {
          $bypass = TRUE;
        }

      }
      else {
        $bypass = TRUE;
      }

    }

  }

  if (!$bypass) {
    $original_router_item_access_callback = $router_item['access_callback'];
    $router_item['access_callback'] = '';
    drupal_alter('site_pages_access_deny_callback', $router_item, $path, $original_map, $original_router_item_access_callback);
  }

}

/**
 * Not zero.
 */
function _not_zero($val) {

  return ($val != '0' && $val != '0');
}

/**
 * Not null.
 */
function _not_null($val) {

  return ($val != NULL && $val != NULL);
}

/**
 * Recursive traversal over multidimensional array while dropping empty values
 * an sub-arrays.
 */
function array_filter_recursive($array, $callback = NULL) {

  foreach ($array as $key => & $value) {
    if (is_array($value)) {
      $value = array_filter_recursive($value, $callback);
    }
    else {
      if (!is_null($callback)) {
        if (!call_user_func($callback, $value)) {
          unset($array[$key]);
        }
      }
      else {
        if (!(bool) $value) {
          unset($array[$key]);
        }
      }
    }
  }
  unset($value);

  return $array;
}

/**
 * Update rules.
 */
function _update_bypass_link_rules_table($form, &$form_state) {

  $pages_rules = $form_state['input']['site_pages_access_table'];

  foreach ($pages_rules as $menu_name => $mlids) {
    foreach ($mlids as $mlid => $rules) {
      foreach ($rules as $key => $value) {
        if ($value === NULL || $key !== $value) {
          unset($pages_rules[$menu_name][$mlid][$key]);
        }
      }
    }
  }

  $rules = variable_get('site_pages_access_rules', '');

  $flatern_pages_rules = [];
  foreach ($pages_rules as $menu) {
    foreach ($menu as $mlid => $page_rules) {
      $flatern_pages_rules[$mlid] = $page_rules;
    }
  }

  $new_flatern_pages_rules = [];
  foreach ($rules as $rule) {
    foreach ($flatern_pages_rules as $mlid => $rs) {
      if (!empty($flatern_pages_rules[$mlid])) {
        foreach ($rs as $key => $val) {
          if ($rule['title'] == $key) {
            foreach ($rule['rules'] as $type => $isset) {
              if ($isset) {
                $new_flatern_pages_rules[$mlid][$key][$type] = $rule[$type];
              }
            }
          }
        }
      }
      else {
        $new_flatern_pages_rules[$mlid] = [];
      }
    }
  }

  variable_set('site_pages_access_flatern_pages_rules', $new_flatern_pages_rules);

}
